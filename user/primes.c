#include "kernel/types.h"
#include "user/user.h"

void
pipeline()
{
    int p[2];
    int prime;

    if (read(0, &prime, 4) == 0) {
        exit(0);
    }
    printf("prime %d\n", prime);

    pipe(p);

    if (fork() == 0) {
        close(0);
        dup(p[0]);
        close(p[0]);
        close(p[1]);

        pipeline();
    } else {
        close(p[0]);

        int number;
        while (read(0, &number, 4) == 4) {
            if (number % prime != 0) {
                write(p[1], &number, 4);
            }
        }

        close(p[1]);

        wait((int*)0);
    }
    exit(0);
}

int
main(int argc, char *argv[])
{
    int p[2];

    pipe(p);

    if (fork() == 0) {
        close(0);
        dup(p[0]);
        close(p[0]);
        close(p[1]);

        pipeline();
    } else {
        close(p[0]);

        for (int i = 2; i <= 35; i++) {
            write(p[1], &i, 4);
        }
        close(p[1]);

        wait((int *)0);
    }
    exit(0);
}

