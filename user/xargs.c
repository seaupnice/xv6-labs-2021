#include "kernel/types.h"
#include "user/user.h"
#include "kernel/param.h"

int
main(int argc, char *argv[])
{
    char *arguments[MAXARG], buf[512];

    if(argc < 2) {
        fprintf(2, "Usage: xargs command\n");
        exit(1);
    }

    if (argc + 1 > MAXARG) {
        fprintf(2, "xargs: argument too much\n");
        exit(1);
    }

    for (int i = 1; i < argc; i++)
        arguments[i-1] = argv[i];

    /* 来自pipeline的左树的结果，左树的fd=1绑定了shell中管道的写端，
    ** pipeline右树的fd=0绑定了shell中管道的读端。
    ** 这里从fd=0中读取左树的结果
    */
    char *p = buf;
    while (read(0, p, 1) == 1) {
        if (*p == '\n') {
            *p = '\0';
            arguments[argc-1] = buf;

            if (fork() == 0) {
                exec(argv[1], arguments);

                fprintf(2, "exec %s failed\n", argv[1]);
                exit(0);
            } else {
                wait((int*)0);
            }

            p = buf;
        } else {
            p++;
        }
    }

    exit(0);
}