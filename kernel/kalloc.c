// Physical memory allocator, for user processes,
// kernel stacks, page-table pages,
// and pipe buffers. Allocates whole 4096-byte pages.

#include "types.h"
#include "param.h"
#include "memlayout.h"
#include "spinlock.h"
#include "riscv.h"
#include "defs.h"

void freerange(void *pa_start, void *pa_end);

extern char end[]; // first address after kernel.
                   // defined by kernel.ld.

struct run {
  struct run *next;
};

struct {
  struct spinlock lock;
  uint16 refcnt[KERPHYPAGE];
} kpageref;

struct {
  struct spinlock lock;
  struct run *freelist;
} kmem[NCPU];

char kmemlockname[NCPU][6];

void
kinit()
{
  initlock(&kpageref.lock, "kpageref");
  for (int i = 0; i < NCPU; i++) {
    snprintf(kmemlockname[i], 6, "kmem-%d", i);
    initlock(&kmem[i].lock, "kmem");
  }
  freerange(end, (void*)PHYSTOP);
}

void
freerange(void *pa_start, void *pa_end)
{
  char *p;
  int phypageindex;
  p = (char*)PGROUNDUP((uint64)pa_start);
  for (; p + PGSIZE <= (char*)pa_end; p += PGSIZE) {
    phypageindex = PHYPAGEINDEX((uint64)p);
    kpageref.refcnt[phypageindex] = 1;
    kfree(p);
  }
}

void
krefinc(uint64 pa)
{
  int phypageindex = PHYPAGEINDEX(pa);

  acquire(&kpageref.lock);
  if (pa >= PHYSTOP || kpageref.refcnt[phypageindex] < 1)
    panic("krefinc pa invalid.");
  kpageref.refcnt[phypageindex]++;
  release(&kpageref.lock);
}

uint16
krefnum(uint64 pa)
{
  int phypageindex = PHYPAGEINDEX(pa);
  uint16 sz;

  acquire(&kpageref.lock);
  sz = kpageref.refcnt[phypageindex];
  release(&kpageref.lock);

  return sz;
}

// Free the page of physical memory pointed at by v,
// which normally should have been returned by a
// call to kalloc().  (The exception is when
// initializing the allocator; see kinit above.)
void
kfree(void *pa)
{
  struct run *r;

  if(((uint64)pa % PGSIZE) != 0 || (char*)pa < end || (uint64)pa >= PHYSTOP)
    panic("kfree");


  int phypageindex = PHYPAGEINDEX((uint64)pa);

  acquire(&kpageref.lock);
  uint16 refcnt = kpageref.refcnt[phypageindex];
  if (refcnt < 1)
    panic("kfree ref = 0.");

  kpageref.refcnt[phypageindex]--;
  release(&kpageref.lock);

  if (refcnt > 1)
    return;

  // Fill with junk to catch dangling refs.
  memset(pa, 1, PGSIZE);

  r = (struct run*)pa;

  push_off();
  int corenum = cpuid();

  acquire(&kmem[corenum].lock);
  r->next = kmem[corenum].freelist;
  kmem[corenum].freelist = r;
  release(&kmem[corenum].lock);

  pop_off();
}

// Allocate one 4096-byte page of physical memory.
// Returns a pointer that the kernel can use.
// Returns 0 if the memory cannot be allocated.
void *
kalloc(void)
{
  struct run *r;

  push_off();
  int corenum = cpuid();

  acquire(&kmem[corenum].lock);
  r = kmem[corenum].freelist;
  if (r) {
    kmem[corenum].freelist = r->next;
  } else {
    // steal
    for (int i = 0; i < NCPU; i++) {
      if (i != corenum) {
        acquire(&kmem[i].lock);
        if (kmem[i].freelist) {
          r = kmem[i].freelist;
          kmem[i].freelist = r->next;
          release(&kmem[i].lock);
          break;
        }
        release(&kmem[i].lock);
      }
    }
  }
  release(&kmem[corenum].lock);
  pop_off();

  if (r) {
    acquire(&kpageref.lock);
    int phypageindex = PHYPAGEINDEX((uint64)r);
    if (kpageref.refcnt[phypageindex] != 0)
      panic("kalloc page ref on 0.");
    kpageref.refcnt[phypageindex] = 1;
    release(&kpageref.lock);
  }

  if(r)
    memset((char*)r, 5, PGSIZE); // fill with junk
  return (void*)r;
}

// collect the amount of free memory.
// Returns the number of bytes of free memory.
uint64
kremainsize(void)
{
    struct run *r;
    uint64 freepg = 0;

    push_off();
    int corenum = cpuid();

    acquire(&kmem[corenum].lock);
    r = kmem[corenum].freelist;
    while (r) {
        freepg++;
        r = r->next;
    }
    release(&kmem[corenum].lock);

    return freepg << 12;
}
