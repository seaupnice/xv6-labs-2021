#include "types.h"
#include "riscv.h"
#include "param.h"
#include "defs.h"
#include "date.h"
#include "memlayout.h"
#include "spinlock.h"
#include "proc.h"

uint64
sys_exit(void)
{
  int n;
  if(argint(0, &n) < 0)
    return -1;
  exit(n);
  return 0;  // not reached
}

uint64
sys_getpid(void)
{
  return myproc()->pid;
}

uint64
sys_fork(void)
{
  return fork();
}

uint64
sys_wait(void)
{
  uint64 p;
  if(argaddr(0, &p) < 0)
    return -1;
  return wait(p);
}

uint64
sys_sbrk(void)
{
  int addr;
  int n;

  if(argint(0, &n) < 0)
    return -1;

  addr = myproc()->sz;
  if(growproc(n) < 0)
    return -1;
  return addr;
}

uint64
sys_sleep(void)
{
  int n;
  uint ticks0;


  if(argint(0, &n) < 0)
    return -1;
  acquire(&tickslock);
  ticks0 = ticks;
  while(ticks - ticks0 < n){
    if(myproc()->killed){
      release(&tickslock);
      return -1;
    }
    sleep(&ticks, &tickslock);
  }
  backtrace();
  release(&tickslock);
  return 0;
}

int
sys_pgaccess(void)
{
  uint64 base;
  int len;
  uint64 mask;

  if(argaddr(0, &base) < 0 || argint(1, &len) < 0
      || argaddr(2, &mask) < 0)
    return -1;

  // set an upper limit to unsigned int mask
  if (len > 32) {
    return -1;
  }

  return pgaccess(base, len, mask);
}

uint64
sys_kill(void)
{
  int pid;

  if(argint(0, &pid) < 0)
    return -1;
  return kill(pid);
}

// return how many clock tick interrupts have occurred
// since start.
uint64
sys_uptime(void)
{
  uint xticks;

  acquire(&tickslock);
  xticks = ticks;
  release(&tickslock);
  return xticks;
}

// Set mask that specify which system calls to trace.
uint64
sys_trace(void)
{
    int tracemask;

    if(argint(0, &tracemask) < 0)
      return -1;

    myproc()->tracemask = tracemask;

    return 0;
}

// Get the number of bytes of free memory,
// and the number of processes whose state is not UNUSED.
uint64
sys_sysinfo(void)
{
    uint64 uinfop; // user pointer to struct sysinfo

    if(argaddr(0, &uinfop) < 0)
        return -1;

    return setsysinfo(uinfop);
}

// After every n "ticks" of CPU time that the program consumes,
// the kernel should call fn.
uint64
sys_sigalarm(void)
{
  int n;
  uint64 addr;

  if (argint(0, &n) < 0 || argaddr(1, &addr) < 0)
    return -1;

  myproc()->alarminterval = n;
  myproc()->alarmpassed = 0;
  myproc()->alarmisreturn = 1;
  myproc()->alarmhandler = (void(*)())addr;

  return 0;
}

uint64
sys_sigreturn(void)
{
  *(myproc()->trapframe) = myproc()->histrapframe;
  myproc()->alarmpassed = 0;
  myproc()->alarmisreturn = 1;
  return 0;
}